//
//  AppDelegate.h
//  ObjectiveCScrollView
//
//  Created by Canyon  on 8/31/17.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

